function initPopupHeight() {
  // var bodyHeight = $("body").height();
  var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
  $('.wishroundMerchantCloseBtn').on('click', function () {
    parent.window.postMessage("removetheiframe", "*");
  });
}

function initClosePopupButton() {
  $('.md-close').on('click', function () {
    $("body").removeClass("is-popup");
    $("html").removeClass("is-popup");
    $(".md-modal").removeClass("md-show");
  });
}

// 
function wishroundMerchantEmailFriendIsLoad() {
  $("#wishroundMerchantEmailFriendRow").addClass("is-load");
}
function wishroundMerchantEmailFriendIsNoLoad() {
  $("#wishroundMerchantEmailFriendRow").removeClass("is-load");
}

function wishroundMerchantEmailFriendOpenPass() {
  $(".js-validateform2").addClass("is-open-pass");
}

//
function wishroundMerchantEmailIsLoad() {
  $("#wishroundMerchantEmailMyRow").addClass("is-load");
}
function wishroundMerchantEmailIsNoLoad() {
  $("#wishroundMerchantEmailMyRow").removeClass("is-load");
}

function wishroundMerchantEmailOpenPass() {
  $(".js-validateform3").addClass("is-open-pass");
}

// Кросс-браузерная функция для получения символа из события keypress:
// больше инфы тут https://learn.javascript.ru/keyboard-events#getChar

function getChar(event) {
  if (event.which == null) { // IE
    if (event.keyCode < 32) return null; // спец. символ
    return String.fromCharCode(event.keyCode)
  }

  if (event.which != 0 && event.charCode != 0) { // все кроме IE
    if (event.which < 32) return null; // спец. символ
    return String.fromCharCode(event.which); // остальные
  }

  return null; // спец. символ
}

$(document).ready(function () {

	initPopupHeight();
	initClosePopup();
  initClosePopupButton();

  // datepicker
  $(function(){
    $('.js-datepicker').datepicker({
      minDate: 0,
      showOtherMonths: true,
      selectOtherMonths: false
    });
  });

  // валидация первой формы с почтой
  $( "#wishroundMerchantEmailFriend" ).keyup(function(e) {
    var myValue = this.value;
    // console.log(myValue);
    if (myValue.length > 3) {
      wishroundMerchantEmailFriendIsLoad();
    } else {
      wishroundMerchantEmailFriendIsNoLoad();
    }

    if (myValue.length > 5) {
      wishroundMerchantEmailFriendOpenPass();
    }
  });

  // валидация второй формы с почтой
  $( "#wishroundMerchantEmail" ).keyup(function(e) {
    var myValue = this.value;
    // console.log(myValue);
    if (myValue.length > 3) {
      wishroundMerchantEmailIsLoad();
    } else {
      wishroundMerchantEmailIsNoLoad();
    }

    if (myValue.length > 5) {
      wishroundMerchantEmailOpenPass();
    }
  });

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// switcher
	$('.js-switchoption .switchoption-item').on('click', function() {
    var item = $("#form-name-my label");

		if ($(this).attr("id") === "friend" ) {
			$(".js-validateform").addClass("is-switch");
		}

		if ($(this).attr("id") === "for-me" ) {
			$(".js-validateform").removeClass("is-switch");
		}
	});

  // tooltip hint
  $( ".js-tooltip-hint1" ).hover(
    function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeIn(400);
    }, function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeOut(400);
    }
  );

  // tooltip hint
  $( ".js-tooltip-hint2" ).hover(
    function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeIn(400);
    }, function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeOut(400);
    }
  );

  // validate form
  $('.js-sendform').on('click', function () {
    $('.js-validateform .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
            input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    // 
    $('.js-validateform .textarea textarea').each(function () {
      var textarea = $(this);
      var val = textarea.val();
      var form = $(".js-validateform");

      // validation textarea
      if (val != "") {
        textarea.addClass("is-succes");
        textarea.removeClass("is-error");
      } else {
        textarea.addClass("is-error");
        textarea.removeClass("is-succes");
      }

      // textarea animation
      if (textarea.hasClass("is-error")) {
        textarea.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
            textarea.parent().removeClass("wobble animated");
        }, 800)
      } else {
        textarea.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform .input input");
    var textarea = $(".js-validateform .textarea textarea");
    var form = $(".js-validateform");

    // validation form ( && textarea )
    if (!$(input).hasClass("is-error") && !$(textarea).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // NEXT STEP
    if ($(".js-validateform").hasClass("is-succes")) {
      if ($(".js-validateform.m-switch").hasClass("is-switch")) {
        console.log('xxx');
        $("#wishroundMerchantAlmostReadyFriend").addClass("md-show");
        $("#wishroundMerchantCreate").removeClass("md-show");
      } else {
        console.log('vvv');
        $("#wishroundMerchantAlmostReadyMe").addClass("md-show");
        $("#wishroundMerchantCreate").removeClass("md-show");
      }

    }
    return false;
  });
  // validate form
  $('.js-sendform2').on('click', function () {

    $('.js-validateform2 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform2");

      // need this if for validate password input
      // если у js-validateform2 появилось поле с паролем
      if (!$(".js-validateform2").hasClass("is-open-pass")) {
        // 
        if (!$(input).hasClass("is-password")) {

          if (val != "") {
            input.addClass("is-succes");
            input.removeClass("is-error");
          } else {
            input.addClass("is-error");
            input.removeClass("is-succes");
          }

        }
      }
      // если для js-validateform2 мы подтянули с БД почту юзера
      else {
        if (val != "") {
          input.addClass("is-succes");
          input.removeClass("is-error");
        } else {
          input.addClass("is-error");
          input.removeClass("is-succes");
        }
      }
      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform2 .input input");
    var form = $(".js-validateform2");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform2").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyFriend").removeClass("md-show");
      $("#wishroundMerchantReadyFriend").addClass("md-show");
    }
    return false;
  });
  // validate form
  $('.js-sendform3').on('click', function () {

    $('.js-validateform3 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform3");

      // 
      if (!$(".js-validateform3").hasClass("is-open-pass")) {
        // 
        if (!$(input).hasClass("is-password")) {

          // validation input
          if (val != "") {
            input.addClass("is-succes");
            input.removeClass("is-error");
          } else {
            input.addClass("is-error");
            input.removeClass("is-succes");
          }

        }
      }

      // если для js-validateform3 мы подтянули с БД почту юзера
      else {
        if (val != "") {
          input.addClass("is-succes");
          input.removeClass("is-error");
        } else {
          input.addClass("is-error");
          input.removeClass("is-succes");
        }
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform3 .input input");
    var form = $(".js-validateform3");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform3").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyMe").removeClass("md-show");
      $("#wishroundMerchantReadyMy").addClass("md-show");
    }
    return false;
  });

  // validate form
  $('.js-sendform4').on('click', function () {

    $('.js-validateform4 .textarea textarea').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform4");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform4 .textarea textarea");
    var form = $(".js-validateform4");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform4").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyFriend").removeClass("md-show"); // ?
      $("#wishroundMerchantSend").removeClass("md-show");
      $("#wishroundMerchantLettrsSend").addClass("md-show");
    }
    return false;
  });

  $('.js-merchant-create').on('click', function () {
    $("#checkoutHowWork").removeClass("md-show");
    $("#wishroundMerchantCreate").addClass("md-show");
  });

  $('.js-open-checkoutHowWork').on('click', function() {
    $("#wishroundMerchantCreate").removeClass("md-show");
    $("#checkoutHowWork").addClass("md-show");
  });

  $('.js-open-wishroundMerchantCreate').on('click', function() {
    $("#wishroundMerchantAlmostReadyMe").removeClass("md-show");
    $("#wishroundMerchantAlmostReadyFriend").removeClass("md-show");
    $("#wishroundMerchantCreate").addClass("md-show");

    // $("#wishroundMerchantEmailMyRow").removeClass("is-load");
  });

  $('.wishroundMerchantReadyEdit').on('click', function() {
    $("#wishroundMerchantReadyMy").removeClass("md-show");
    $("#wishroundMerchantReadyFriend").removeClass("md-show");
    $("#wishroundMerchantCreate").addClass("md-show");
  });

  $('#btn-share-on-facebook').on('click', function() {
    $("#wishroundMerchantReadyMy").removeClass("md-show");
    $("#wishroundMerchantWishPosted").addClass("md-show");
  });
  
  $('.wishroundMerchantMail').on('click', function() {
    $("#wishroundMerchantReadyFriend").removeClass("md-show");
    $("#wishroundMerchantSend").addClass("md-show");
  });

  $('.js-open-ready-friend').on('click', function() {
    $("#wishroundMerchantSend").removeClass("md-show");
    $("#wishroundMerchantReadyFriend").addClass("md-show");
  });

  //
  $('.js-send-mail').on('click', function() {
    $("#wishroundMerchantReadyFriend").removeClass("md-show");
    $("#wishroundMerchantSend").addClass("md-show");
  });
  
  $('.js-send-mail-my').on('click', function() {
    $("#wishroundMerchantReadyMy").removeClass("md-show");
    $("#wishroundMerchantSend").addClass("md-show");

    $('.js-open-ready-friend').on('click', function() {
      $("#wishroundMerchantReadyMy").addClass("md-show");
      $("#wishroundMerchantSend").removeClass("md-show");
    });
  });
  
  // one wish
  $('.one-wish-item').on('click', function () {
    $(this).addClass('is-active').siblings().removeClass('is-active');
  });

});

