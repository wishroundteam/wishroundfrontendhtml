function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
function initClosePopup() {
	$('.md-close').on('click', function () {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function () {
		// skip birthday and email popup do disallow closing
		if ($('#birthday').hasClass('md-show') !== true) {
			$(".md-modal").removeClass("md-show");
			$("body").removeClass("is-popup");
			$("html").removeClass("is-popup");
			$(".nav-mobile").removeClass("is-active");
		}
	});
}
function openPopupDeleteAcc() {
	$("#delete-account").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}

function initPopupDeleteAcc() {
	$('#popupDeleteAcc').on('click', function () {
		if ($(this).hasClass("md-trigger")) {
			openPopupDeleteAcc();
			return false;
		}
	});
}

function tooltipFbErrorShow() {
	$(".js-tooltip-fb-error").fadeIn(400);
}
function tooltipFbErrorClose() {
	$(".js-tooltip-fb-error").stop();
	$(".js-tooltip-fb-error").fadeOut(400);
}
function tooltipTwErrorShow() {
	$(".js-tooltip-tw-error").fadeIn(400);
}
function tooltipTwErrorClose() {
	$(".js-tooltip-tw-error").stop();
	$(".js-tooltip-tw-error").fadeOut(400);
}

/*
	BEGIN
	VALIDATE PHONE
*/
function phoneVerificationClear () {
	var row                       = $(".js-phone-verification");
	var phoneVerificationInput    = $(".js-phone-verification input");
	var errorMessage              = $(".phone-verification-message-error");

	row.removeClass("is-error");
	row.removeClass("is-load");
	phoneVerificationInput.attr("disabled", false);
	errorMessage.remove();
}

function phoneVerificationIsError () {
	var row                       = $(".js-phone-verification");
	var phoneVerificationMessage  = $(".js-phone-verification-message");
	var errorMessage              = "<p class='phone-verification-message-error is-error'>Error message here, validation error</p>";

	// clear other status
	phoneVerificationClear ();

	//add "is-error" status
	phoneVerificationMessage.append(errorMessage);
	row.addClass("is-error");
}

function phoneVerificationIsLoad() {
	var row                       = $(".js-phone-verification");
	var phoneVerificationInput    = $(".js-phone-verification input");
	var label                     = $(".js-phone-verification label");

	// clear other status
	phoneVerificationClear ();

	//add "is-load" status
	row.addClass("is-load");
	label.html("Wait checkin your number");
	phoneVerificationInput.attr("disabled", true);
}

function phoneVerificationZipCode() {
	var phoneVerificationMessage  = $(".js-phone-verification-message");
	var label                     = $(".js-phone-verification label");
	var errorMessage              = "<p class='phone-verification-message-error'>В течении 5 минут выполучите смс<br>с кодом подтверждения. <a href='#' class='phone-verification-send-code'>Выслать повторно</a></p>";

	// clear other status
	phoneVerificationClear ();

	//add "is-zip-code" status
	label.html("Input your code");
	phoneVerificationMessage.append(errorMessage);
}

function phoneVerificationZipCodeIsError() {
	var row                       = $(".js-phone-verification");
	var phoneVerificationMessage  = $(".js-phone-verification-message");
	var errorMessage              = "<p class='phone-verification-message-error is-error'>В течении 5 минут выполучите смс<br>с кодом подтверждения. <a href='#' class='phone-verification-send-code'>Выслать повторно</a></p>";

	// clear other status
	phoneVerificationClear ();

	//add "is-error" status
	phoneVerificationMessage.append(errorMessage);
	row.addClass("is-error");
}

function phoneVerificationSuccessCheck() {
	var row                       = $(".js-phone-verification");
	var label                     = $(".js-phone-verification label");
	var phoneVerificationInput    = $(".js-phone-verification input");

	// clear other status
	phoneVerificationClear ();

	//add "is-success" status
	label.html("Success checking");
	row.addClass("is-success");
	phoneVerificationInput.attr("disabled", true);
}

function phoneVerificationInputFocusStart() {
	var phoneVerificationFloatiogPlaceholder    = $(".js-phone-verification .floating-placeholder");

	phoneVerificationFloatiogPlaceholder.addClass("floating-placeholder-float");
}
function phoneVerificationInputFocusEnd() {
	var phoneVerificationFloatiogPlaceholder    = $(".js-phone-verification .floating-placeholder");

	phoneVerificationFloatiogPlaceholder.removeClass("floating-placeholder-float");
}

/*
	END
	VALIDATE PHONE
*/

$(document).ready(function() {

	initPopupHeight();
	initClosePopup();

	initPopupDeleteAcc();

	// phone mask
	$('#phone-verification') 

		.keydown(function (e) {
			var key = e.charCode || e.keyCode || 0;
			$phone = $(this);

			// Auto-format- do not expose the mask as the user begins to type
			if (key !== 8 && key !== 9) {
				if ($phone.val().length === 7) {
					$phone.val($phone.val() + ')');
				}
				if ($phone.val().length === 8) {
					$phone.val($phone.val() + ' ');
				}			
				if ($phone.val().length === 12) {
					$phone.val($phone.val() + '-');
				}
				if ($phone.val().length === 15) {
					$phone.val($phone.val() + '-');
				}
			}

			// Allow numeric (and tab, backspace, delete) keys only
			return (key == 8 || 
					key == 9 ||
					key == 46 ||
					(key >= 48 && key <= 57) ||
					(key >= 96 && key <= 105));	
		})
		
		.bind('focus click', function () {

			// focus
			phoneVerificationInputFocusStart();

			$phone = $(this);
			
			if ($phone.val().length === 0) {
				$phone.val('+38(');
			}
			else {
				var val = $phone.val();
				$phone.val('').val(val); // Ensure cursor remains at the end
			}
		})
		
		.blur(function () {

			$phone = $(this);

			// reset mask
			if ($phone.val().length < 5) {
				$phone.val('');
				phoneVerificationInputFocusEnd();
			}
			
			if ($phone.val() === '(') {
				$phone.val('');
			}
		});



	// validate form
	$('.js-connect-success').on('click', function () {
		$('.js-validate-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validate-form");

			// validation input
			if (val != "") {
					input.addClass("is-succes");
					input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.parent().addClass("wobble animated");

					var delay = setTimeout(function () {
							input.parent().removeClass("wobble animated");
					}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validate-form .input input");
		var form = $(".js-validate-form");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		return false;
	});

	// validate form
	$('.js-connect-success2').on('click', function () {
		$('.js-validate-form2 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validate-form2");

			// validation input
			if (val != "") {
					input.addClass("is-succes");
					input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.parent().addClass("wobble animated");

					var delay = setTimeout(function () {
							input.parent().removeClass("wobble animated");
					}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validate-form2 .input input");
		var form = $(".js-validate-form2");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		return false;
	});

	// validate form
	$('.js-connect-success3').on('click', function () {
		$('.js-validate-form3 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validate-form3");

			// validation input
			if (val != "") {
					input.addClass("is-succes");
					input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.parent().addClass("wobble animated");

					var delay = setTimeout(function () {
							input.parent().removeClass("wobble animated");
					}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validate-form3 .input input");
		var form = $(".js-validate-form3");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		return false;
	});


	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});
	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});
	// tab 
	function tab() {
		$(".js-tab").each(function(){
			var tab_link = $(this).find("a");
			var tab_item = $(this).find("li");
			var tab_cont = $(this).parents(".js-tab-group").find(".js-tab-cont");
			// tab_cont.hide(); 
			tab_item.first().addClass("is-active");
			$(this).parents(".js-tab-group").find(".js-tab1").addClass("is-active");
			tab_link.on("click", function() {
				var index = $(this).attr("href");
				tab_item.removeClass("is-active");
				$(this).parent().addClass("is-active");
				tab_cont.removeClass("is-active");
				$(this).parents(".js-tab-group").find("."+index).addClass("is-active");
				return false;
			});
			});
	} tab();
	// tooltip hint1
	$(".js-tooltip-hint1").hover(
		function () {
			$(".tooltip-hint1").stop();
			$(".tooltip-hint1").fadeIn(400);
		}, function () {
			$(".tooltip-hint1").stop();
			$(".tooltip-hint1").fadeOut(400);
		}
	);
	// tooltip hint2
	$(".js-tooltip-hint2").hover(
		function () {
			$(".tooltip-hint2").stop();
			$(".tooltip-hint2").fadeIn(400);
		}, function () {
			$(".tooltip-hint2").stop();
			$(".tooltip-hint2").fadeOut(400);
		}
	);
	// tooltip hint3
	$(".js-tooltip-hint3").hover(
		function () {
			$(".tooltip-hint3").stop();
			$(".tooltip-hint3").fadeIn(400);
		}, function () {
			$(".tooltip-hint3").stop();
			$(".tooltip-hint3").fadeOut(400);
		}
	);

	// tooltip Twitter error


	// tooltip Twitter error

	// 
	Dropzone.autoDiscover = true;
	Dropzone.options.fbDropZone = {
			paramName: "file",
			addRemoveLinks: "dictRemoveFile",
			thumbnailWidth: 130,
			thumbnailHeight: 130,
			maxFilesize: 10,
			maxFiles: 1,
			autoProcessQueue: true,
			uploadMultiple: false,
			acceptedFiles: '.png,.jpg,.gif,.jpeg'
	};
	// remove img
	$(".fa-trash").on('click', function (e) {
		$(".dz-preview").remove();
		return false;
	});

	/*jslint  browser: true, white: true, plusplus: true */
	/*global $, countries */
	$(function () {
			'use strict';

			var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });

			// Setup jQuery ajax mock:
			$.mockjax({
					url: '*',
					responseTime: 2000,
					response: function (settings) {
							var query = settings.data.query,
									queryLowerCase = query.toLowerCase(),
									re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi'),
									suggestions = $.grep(countriesArray, function (country) {
											 // return country.value.toLowerCase().indexOf(queryLowerCase) === 0;
											return re.test(country.value);
									}),
									response = {
											query: query,
											suggestions: suggestions
									};

							this.responseText = JSON.stringify(response);
					}
			});

			// Initialize ajax autocomplete:
			$('#autocomplete-ajax').autocomplete({
					// serviceUrl: '/autosuggest/service/url',
					lookup: countriesArray,
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
							var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
							return re.test(suggestion.value);
					},
					onSelect: function(suggestion) {
							$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
					},
					onHint: function (hint) {
							$('#autocomplete-ajax-x').val(hint);
					},
					onInvalidateSelection: function() {
							$('#selction-ajax').html('You selected: none');
					}
			});

			// Initialize ajax autocomplete:
			$('#autocomplete-ajax2').autocomplete({
					// serviceUrl: '/autosuggest/service/url',
					lookup: countriesArray,
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
							var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
							return re.test(suggestion.value);
					},
					onSelect: function(suggestion) {
							$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
					},
					onHint: function (hint) {
							$('#autocomplete-ajax-x').val(hint);
					},
					onInvalidateSelection: function() {
							$('#selction-ajax').html('You selected: none');
					}
			});

	});

});