function openPopupHowWork() {
	$("#how-work").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupPartners() {
	$("#partners").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		// $(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		// $(".nav-mobile").removeClass("is-active");
	});
}
function initPopupHowWork() {
	var link = $("a[data-modal='how-work']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupHowWork();
			return false;
		}
	});
}
function initPopupPartners() {
	var link = $("a[data-modal='partners']");

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupPartners();
			return false;
		}
	});
}

function openPopupCloseWish() {
	$("#close-wish").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}

function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		// $(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".nav-mobile").removeClass("is-active");
	});
}

function initPopupCloseWish() {
	var link = $("a[data-modal='close-wish']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupCloseWish();
			return false;
		}
	});
}

// function initPopupAskMeLater() {
// 	var link = $(".js-ask-later");

// 	link.on('click', function() {
// 		$("#close-wish").removeClass("md-show");
// 		$("body").removeClass("is-popup");
// 		$("html").removeClass("is-popup");

// 		$(".js-wishinfo").addClass("is-active");
// 		$(".wishoverlay").addClass("is-active");
// 		$(".wish").addClass("is-overlay");


// 		// return false;
// 	});
// }



$(document).ready(function () {
	initPopupHeight();
	initClosePopup();
	// initPopupMobile();

	initPopupHowWork();
	initPopupPartners();

	initPopupCloseWish();
	// initPopupAskMeLater();

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});
	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	// tooltip hint
	$( ".js-tooltip-hint" ).hover(
		function() {
			$(".ranger-tooltip").stop();
			$(".ranger-tooltip").fadeIn(400);
		}, function() {
			$(".ranger-tooltip").stop();
			$(".ranger-tooltip").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

	$(function(){
		var slideshows = $('.cycle-slideshow').on('cycle-prev cycle-next', function(e, opts) {
		    // advance the other slideshow
		    slideshows.not(this).cycle('goto', opts.currSlide);
		});

		var slideshows2 = $('.cycle-slideshow').on('cycle-pager-activated', function(e, opts) {
		    // advance the other slideshow
		    slideshows2.not(this).cycle('goto', opts.currSlide);
		});

		$('#cycle-2 .cycle-slide').click(function(){
		    console.log("clicked");
		    var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
		    slideshows.cycle('goto', index);
		});

	});

});