function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
  $('#wishroundMangopayCloseBtn').on('click', function () {
    parent.window.postMessage("removetheiframe", "*");
  });
}

function delIframe() {
	parent.window.postMessage("removetheiframe", "*");

	// $('#wishroundIframeOverlay', window.parent.document).parent().find("#js-day").removeClass("is-active");
	// $('#wishroundIframeOverlay', window.parent.document).parent().find("#js-congrats").addClass("is-active");
}

$(document).ready(function () {
	initPopupHeight();
	initClosePopup();

	// validate form 
	$('.js-validate-form').on('click', function () {
    $('.js-payment-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-payment-form");

			// validation input
			if (val != "") {
			    input.addClass("is-succes");
			    input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
			    input.parent().addClass("wobble animated");

			    var delay = setTimeout(function () {
			        input.parent().removeClass("wobble animated");
			    }, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

    var input = $(".js-payment-form .input input");
    var form = $(".js-payment-form");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		if ($(".js-payment-form").hasClass("is-succes")) {
			delIframe();
		}

		return false;
	});



	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);


	// tooltip
	$( ".js-tooltip" ).hover(
		function() {
			$(".tooltip-payment").stop();
			$(".tooltip-payment").fadeIn(400);
		}, function() {
			$(".tooltip-payment").stop();
		 $(".tooltip-payment").fadeOut(400);
		}
	);


		// open keyboard
		$('#pass').keydown(false);
		//
		$('#js-input-cvv').click(function(e) {
			$(".payment-keyboard").addClass("is-active");
			e.stopPropagation();
			closekeyboard();
		});

		// close keyboard
		function closekeyboard() {
			$(document).click(function(e){
		    if ($(e.target).closest(".payment-keyboard").length == 0) {
	        // .closest can help you determine if the element 
	        // or one of its ancestors is #payment-keyboard
	        $(".payment-keyboard").removeClass("is-active");
	        $("#input-cvv").removeClass("is-focus");	// remove focus state
		    }
			});
			$(window.parent.document).click(function(e){
		    if ($(e.target).closest(".payment-keyboard").length == 0) {
	        $(".payment-keyboard").removeClass("is-active");
	        $("#input-cvv").removeClass("is-focus");
		    }
			});
			$(window.parent.parent.document).click(function(e){
		    if ($(e.target).closest(".payment-keyboard").length == 0) {
	        $(".payment-keyboard").removeClass("is-active");
	        $("#input-cvv").removeClass("is-focus");
		    }
			});
		}
		// close keyboard button
		$('.payment-keyboard-btn.success').on('click', function(e) {
			$(".payment-keyboard").removeClass("is-active");
			$("#input-cvv").removeClass("is-focus");	// remove focus state
		});

		// store value in input
		$('.payment-keyboard-items .payment-keyboard-btn').on('click', function() {
			var item = $(this);
			var input = $("#input-cvv input");

			if (!$(item).hasClass("reset")) {
				if (input.val().length < 4) {
					input.val(input.val()+item.text()); // set input value

					input.parent().addClass("floating-placeholder-float");
					input.parent().addClass("is-focus"); // add focus state
				}
			} else {
				input.val(""); // remove input value
				// input.val(input.val().slice(0, -1)); // remove input value

				// remove focus state
				if ($("#input-cvv input").val().length == 0) {
					$("#input-cvv").removeClass("is-focus");
					$("#input-cvv").removeClass("floating-placeholder-float");
				}
			}
		});

		// mask for input card (4 sumbol)
		$('#card').keyup(function() {
		  var foo = $(this).val().split(" ").join(""); // remove hyphens
		  if (foo.length > 0) {
		    foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
		  }
		  $(this).val(foo);
		});

		// BEGIN card detect !!! REWRITE !!!
		$(function(){ 
			$("#card").focus();
			$("#card").detectCard().on('cardChange', function(e, card){
				$("span#type-card").removeClass().addClass(card.type);
			})
		});
		// END card detect !!! REWRITE !!!

});