function slick() {
	$('.js-partners').slick({
		dots: false,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-partners-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-partners-next'></i>",
		responsive: [
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]

	});
}
function slick2() {
	$('.js-review').slick({
		fade: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		draggable: false,
		swipe: false,
		appendArrows: "",
		dots: true,
		appendDots: ".landing-review-wrap"
	});
}


function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "");
}


$(document).ready(function() {

	slick();
	slick2();

	footerMarginCookie();

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	var player = videojs('really-cool-video', { /* Options */ }, function() {});

	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top + -60 + "px"
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	$('.js-review-next').click(function(){
		$('.js-review').slickNext();
	});
	$('.js-review-prev').click(function(){
		$('.js-review').slickPrev();
	});

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".landing-nav").slideToggle(200);
		$(this).toggleClass("is-active");
		$(".nav-overlay").toggle();
	});
	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".landing-nav").slideUp(200);
		$(".js-nav-mobile").removeClass("is-active");
		$(".nav-overlay").hide();
	});

	function initHeaderBtn() {
		var height1 = $("#section0").height();

		if ( $(window).scrollTop() > height1 ) {
			$("header").addClass("m-mobile-scroll");
		} else { 
			$("header").removeClass("m-mobile-scroll");
		}
	}
	initHeaderBtn();

	$(window).scroll(function () {
		initHeaderBtn();
	});

	$(window).resize(function() {
    footerMarginCookie();
	});

}); 