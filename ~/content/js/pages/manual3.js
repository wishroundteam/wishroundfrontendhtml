function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
function openPopupSendWish() {
	$("#popup-send-wish").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function closePopupSendWish() {
	$("#popup-send-wish").removeClass("md-show");

	$("body").removeClass("is-popup");
	$("html").removeClass("is-popup");
}
function initPopupSendWish() {
	$('.mail-step1-send').on('click', function () {
		if ($(this).hasClass("md-trigger")) {

			openPopupSendWish();
			setTimeout(closePopupSendWish, 1800);

			return false;
		}
	});
}

function bounceInput(input) {
	if (!input.parent().hasClass("wobble animated")) {
	  input.parent().addClass("wobble animated");

	  setTimeout(function () {
	    input.parent().removeClass("wobble animated");
	  }, 900);
	}
}
// bounceInput($("#link4"));
// bounceInput($(".import-table-items"));

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}

$(document).ready(function () {

	initPopupHeight();
	initPopupSendWish();

	footerMarginCookie();

	$(function () {
		$('.mail-step1-back').on('click', function () {
			$(".share").show();
			$(".mail-step1").hide();
			return false;
		});
	});

	$(function () {
		$('.import-title-back').on('click', function () {
			$(".mail-step1").show();
			$(".import").hide();
			return false;
		});
	});

	// sticky footer
	function stickyFooter() {
		var footerHeight = $("footer").height();
		$(".out").css("margin-bottom", -footerHeight);
		$(".push").css("height", footerHeight);
	}
	stickyFooter();

	$(window).resize(function() {
    stickyFooter();
    footerMarginCookie();
	});

	//-refactoring scroll
	function scrollIndex() {
		if ($(window).width() > 980) {
			smoothScroll.init({
				offset: 60
			});
		} else {
			smoothScroll.init({
				offset: 0
			});
		}
	}
	scrollIndex();

	$(function () {
		$('.cookie-succes').on('click', function () {
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});

	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	// tooltip error
	$(".js-tooltip-error").hover(
		function () {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeIn(400);
		}, function () {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function () {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// tooltip hover
	$(".js-tooltip").hover(
		function () {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeIn(400);
			$(this).addClass("is-hover");
		}, function () {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeOut(400);
			$(this).removeClass("is-hover");
		}
	);

	// validate form
	$('.js-sendform').on('click', function () {

		$('.js-validateform .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}

			// validation form
			if (!$(input).hasClass("is-error")) {
				form.addClass("is-succes");
				form.removeClass("is-error");
			} else {
				form.addClass("is-error");
				form.removeClass("is-succes");
			}
		});
		return false;
	});

	$('.js-send-mail').on('click', function () {
		$(".share").hide();
		$(".mail-step1").show();
	});

	$('.js-import').on('click', function () {
		$(".mail-step1").hide();
		$(".import").show();
	});

	// table check
	$('.import-table .import-table-item').on('click', function () {
		$(this).toggleClass("is-active");
		// TOOGLE CHECK
	});

	function popupWindow(url, title, w, h) {
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'directories=no, status=no, resizable=no, copyhistory=no, scrollbars=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		return newWindow;
	}

	var shareOnFacebookXhr = null;
	var facebookPopup = null;
	var facebookPopupTimer = null;

	function checkFacebookPopupWindow() {
		if (facebookPopup == null || facebookPopup.closed === true) {
			clearInterval(facebookPopupTimer);
			// TODO: Remove preloader from "Share on Facebook" button
		}
	}

	function checkFacebookPopupCallback(data) {
		if (data) {
			if (data.Success === true) {
				// TODO: Remove preloader from "Share on Facebook" button
				// TODO: Check is it login callback or share callback!
			}
		}
		facebookPopup.close();
		// TODO: Remove preloader from "Share on Facebook" button
	}

	if (!window.checkFacebookPopupCallback) {
		window.checkFacebookPopupCallback = checkFacebookPopupCallback;
	}

	$('#btn-share-on-facebook').on('click', function() {

		// TODO: Start preloader on sign-up popup

		if (shareOnFacebookXhr !== null) {
			shareOnFacebookXhr.abort();
		}

		var d = {
			wishId: WISH_ID
		};

		if (facebookPopup === null || facebookPopup === undefined || facebookPopup.closed === true) {
			if (facebookPopupTimer != null) {
				clearInterval(facebookPopupTimer);
				facebookPopupTimer = null;
			}
			facebookPopupTimer = setInterval(checkFacebookPopupWindow, 500);
			facebookPopup = popupWindow(BASE_URL + 'System/PopupWait', "_blank", 700, 500);
			facebookPopup.focus();
		} else {
			console.log('err #1');
			// TODO: ???
		}

		shareOnFacebookXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmStep3ShareOnFacebook',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						if (response.LoginPopupUrl !== null && response.LoginPopupUrl !== undefined) {
							if (facebookPopup === null || facebookPopup === undefined || facebookPopup.closed === true) {
								// TODO: Remove preloader from "Share on Facebook" button
								return;
							}
							facebookPopup.location = response.LoginPopupUrl;
							if (window.focus) {
								facebookPopup.focus();
							}
						} else if (response.SharePopupUrl !== null && response.SharePopupUrl !== undefined) {
							if (facebookPopup === null || facebookPopup === undefined || facebookPopup.closed === true) {
								// TODO: Remove preloader from "Share on Facebook" button
								return;
							}
							facebookPopup.location = response.SharePopupUrl;
							if (window.focus) {
								facebookPopup.focus();
							}
						} else {
							// TODO: Remove preloader from "Share on Facebook" button
							if (facebookPopup !== null) facebookPopup.close();
						}
					} else {
						// TODO: Remove preloader from "Share on Facebook" button
						if (facebookPopup !== null) facebookPopup.close();
						console.log(response);
					}
				}
			},
			error: function (err) {
				// TODO: Remove preloader from "Share on Facebook" button
				if (facebookPopup !== null) facebookPopup.close();
				console.log(err);
			}
		});

		return false;
	});

	$('#btn-start-short-link-edit').on('click', function () {
		startShortLinkEdit();
	});
	$('#btn-stop-short-link-edit').on('click', function () {
		stopShortLinkEdit(false);
	});

	// TODO: add handler for Esc and Enter
	$('#select-input').alpha({
		allow: '1234567890-_',
		allowSpace: false,
		allowUpper: false,
		allowCaseless: false,
		allowOtherCharSets: false
	});
});

var oldSl = null;
var shortLinkUpdateXhr = null;

function stopShortLinkEdit(skipRequest) {
	var newSl = $('#select-input').val();
	if (newSl !== oldSl && !skipRequest) {
		//if (updEBXhr != null) return false;

		//var d = {
		//    wishId: WISH_ID,
		//    newSl: bDay
		//};

		//var complete = false;

		//// TODO: startPreloaderOnButton($('#btn-next'));

		//updEBXhr = $.ajax({
		//    type: "POST",
		//    url: BASE_URL + 'Ajax/CmStep2UpdateEB',
		//    contentType: "application/json; charset=utf-8",
		//    data: JSON.stringify(d),
		//    dataType: "json",
		//    success: function (response) {
		//        if (response !== null && response !== undefined) {
		//            if (response.Success === true) {
		//                // TODO: Remove popup

		//                complete = true;
		//                $('#btn-next').click();

		//            } else {
		//                console.log(response);
		//            }
		//        }
		//    },
		//    complete: function () {
		//        updEBXhr = null;
		//        if (!complete) {
		//            // TODO: stopPreloaderOnButton($('#btn-next'));
		//        }
		//    }
		//});
		//return false;
	} else {
		newSl = oldSl;
		$('#select-input').val(newSl);
	}

	var fullUrl = 'http://wirnd.com/' + newSl;
	$('#short-link-url').text(fullUrl);
	$('#short-link-link').attr('href', fullUrl);
	$('#short-link-edit-wrap').hide();
	$('#short-link-preview-wrap').show();
}

function startShortLinkEdit() {
	oldSl = $('#select-input').val();

	$('#short-link-preview-wrap').hide();
	$('#short-link-edit-wrap').show();
	document.getElementById("select-input").select();
}