function slick() {
	$('.js-partners').slick({
		dots: false,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-partners-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-partners-next'></i>"
	});
}
function slick2() {
	$('.js-review').slick({
		dots: true,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-review-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-review-next'></i>"
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "");
}

$(document).ready(function() {

	slick();
	slick2();

	footerMarginCookie();

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	function fullpage() {
		// $('body.landing').addClass("is-small-height");
		$('#fullpage').fullpage({
			afterLoad: function(anchorLink, index){
				var loadedSection = $(this);
				//using index
				if(index == 1){
					$("header").removeClass('is-animate');
				}
				if(index == 2){
					$("header").addClass('is-animate');
				}
				if(index == 3){
					$("header").addClass('is-animate');
				}
				if(index == 4){
					$("header").addClass('is-animate');
					videoOpinie();
				}
				if(index == 5){
					$("header").addClass('is-animate');
				}
			},
			anchors: ['Main', 'Video', 'how-it-works', 'Reviews', 'About-service'],
			menu: '.fp-menu',
			scrollingSpeed: 700,
			recordHistory: false
		});
	}

	if ( $(window).height() > 600 &&  $(window).width() > 960 ) {
		$('body.landing').removeClass("is-fullpage-destroy");
		fullpage();

		var player = videojs('really-cool-video', { /* Options */ }, function() {});
		function videoOpinie() {
			var player = videojs('really-cool-video2', { /* Options */ }, function() {
				this.play();
			});
		}
	} else {
		$('body.landing').addClass("is-fullpage-destroy");
		$("header").removeClass('is-animate');
	}

	$(document).on('click', '.js-video-slide', function(){
	  $.fn.fullpage.moveSectionDown();
	});

	$(window).resize(function() {
    if ( $(window).height() > 600 &&  $(window).width() > 960 ) {
  		$.fn.fullpage.destroy('all');
  		$('body.landing').removeClass("is-fullpage-destroy");
      fullpage();

      var player = videojs('really-cool-video', { /* Options */ }, function() {});
      function videoOpinie() {
      	var player = videojs('really-cool-video2', { /* Options */ }, function() {
      		this.play();
      	});
      }
    } else {
    	// fullpage(); // important - create bug
    	$.fn.fullpage.destroy('all');
  		$('body.landing').addClass("is-fullpage-destroy");
  		$("header").removeClass('is-animate');
    }


    footerMarginCookie();
	});



}); 