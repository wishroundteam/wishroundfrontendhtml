function initPopupHeight() {
	// var bodyHeight = $("body").height();
	// var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", "100%");
}

function initClosePopup() {
  $('#wishroundCheckoutCloseBtn').on('click', function () {
    parent.window.postMessage("removetheiframe", "*");
  });
}

function changeIframeHeight() {
	$('.js-checkout-form').on('click', function () {
	  $('#wishroundFrame', window.parent.document).animate({height:'645px'}, 500);
	});
}

function delIframeNote() {
	$('#openCheckoutMail').on('click', function () {
	  $('#wishroundIframeWrap', window.parent.parent.document).find("#wishroundIframeLink").attr("style", "display:none !important");
	});
}

function createIframeNote() {
	$('.js-popup-log').on('click', function () {
	  $('#wishroundIframeWrap', window.parent.parent.document).find("#wishroundIframeLink").attr("style", "font-size: 12px !important;color:#4a94f9 !important;text-decoration: none !important;font-family: 'Open Sans', sans-serif !important;font-weight:300 !important;margin: 20px auto !important;display: inline-block !important;");
	});
}



$(document).ready(function () {

	initPopupHeight();
	initClosePopup();

	changeIframeHeight();

	delIframeNote();
	createIframeNote();

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// tooltip
	$( ".js-tooltip" ).hover(
		function() {
			$(".tooltip-payment").stop();
			$(".tooltip-payment").fadeIn(400);
		}, function() {
			$(".tooltip-payment").stop();
		 $(".tooltip-payment").fadeOut(400);
		}
	);

	// checkoutLog
	// checkoutMail
	// checkoutForgot
	// checkoutPass
	// checkoutPay
	// checkoutPayForm
	$('#openCheckoutMail').on('click', function () {
		$("#checkoutMail").slideDown(); // open
	  $("#checkoutLog").slideUp(); // close
	  $("#checkoutForgot").slideUp(); // close
	  $("#checkoutPass").slideUp(); // close
	  $("#checkoutPay").slideUp(); // close
	  $(".wishroundCheckoutPopup-note").fadeOut(); // close
	});
	//
	$('.js-popup-log').on('click', function () {
	  $("#checkoutLog").slideDown(); // open
	  $("#checkoutMail").slideUp(); //close
	  $("#checkoutForgot").slideUp(); // close
	  $("#checkoutPass").slideUp(); // close
	  $("#checkoutPay").slideUp(); // close
	  $(".wishroundCheckoutPopup-note").fadeIn(); // open
	});
	//
	$('.js-forgot-pass').on('click', function () {
		$("#checkoutForgot").slideDown(); // open
	  $("#checkoutLog").slideUp(); // close
	  $("#checkoutMail").slideUp(); // close
	  $("#checkoutPass").slideUp(); // close
	  $("#checkoutPay").slideUp(); // close
	});
	//
	$('.js-popup-login').on('click', function () {
		$("#checkoutMail").slideDown(); // open
	  $("#checkoutLog").slideUp(); // close
	  $("#checkoutForgot").slideUp(); // close
	  $("#checkoutPass").slideUp(); // close
	  $("#checkoutPay").slideUp(); // close
	  $(".wishroundCheckoutPopup-note").fadeOut(); // close
	});

	// & validate form
	$('.js-checkout-pay').on('click', function () {
		$('.js-payment-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-payment-form");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.parent().addClass("wobble animated");

					var delay = setTimeout(function () {
						input.parent().removeClass("wobble animated");
					}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-payment-form .input input");
		var form = $(".js-payment-form");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// NEXT STEP
		if ($("#checkoutMail").hasClass("is-succes")) {
			$("#checkoutPay").slideDown(); // open
		  $("#checkoutLog").slideUp(); // close
		  $("#checkoutForgot").slideUp(); // close
		  $("#checkoutPass").slideUp(); // close
		  $("#checkoutMail").slideUp(); // close
		}

		return false;
	});
	// validate form
	$('.js-reset-pass').on('click', function () {
		$('#checkoutForgot .checkoutforgot-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $("#checkoutForgot .checkoutforgot-form");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.parent().addClass("wobble animated");

					var delay = setTimeout(function () {
						input.parent().removeClass("wobble animated");
					}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $("#checkoutForgot .checkoutforgot-form  .input input");
		var form = $("#checkoutForgot .checkoutforgot-form");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		// NEXT STEP
		if ($("#checkoutForgot .checkoutforgot-form").hasClass("is-succes")) {
			$("#checkoutPass").slideDown(); // open
			$("#checkoutLog").slideUp(); // close
			$("#checkoutMail").slideUp(); // close
			$("#checkoutForgot").slideUp(); // close
			$("#checkoutPay").slideUp(); // close
		}
		return false;
	});

	//
	$('.js-checkout-form').on('click', function () {
		$("#checkoutPayForm").slideDown(); // open
	  $("#checkoutLog").slideUp(); // close
	  $("#checkoutForgot").slideUp(); // close
	  $("#checkoutPass").slideUp(); // close
	  $("#checkoutMail").slideUp(); // close
	  $("#checkoutPay").slideUp(); // close
	});

	// validate form - REWRITE
	// добавить валидацтю карты (16 значение + диапазоны)
	// добавить валидацию cvv (3-4 значения)

	// если введена карта не валидно, нужно показать тултип
	// tooltip-card-error через fadeIn();

	// если введен cvv не валидно, нужно показать тултип 
	// tooltip-cvv-error через fadeIn();
	$('.js-sendform').on('click', function () {
    $('.js-validateform .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
            input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $('.js-validateform .input input');
    var form = $(".js-validateform");
    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }
    return false;
	});

});




