function slick() {
	$('.js-partners').slick({
		dots: false,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-partners-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-partners-next'></i>"
	});
}

function slick2() {
	$('.js-review').slick({
		dots: true,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-review-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-review-next'></i>"
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "");
}

function videoOpinie() {
	var player = videojs('really-cool-video', { /* Options */ }, function() {
		this.play();
	});
}

function videoOpinieStop() {
	var player = videojs('really-cool-video', { /* Options */ }, function() {
		this.pause();
	});
}

function videoScroll() {
  var position = $("#section3").offset().top;
  var position2 = position - 300;

  var position3 = $("#section4").offset().top;

  if ($(window).scrollTop() >= position3) {
    $(".js-video").hide();
    $(".js-video-note").show();
    videoOpinieStop();
  }
  else if ($(window).scrollTop() >= position2) {
  	$(".js-video").show();
  	$(".js-video-note").hide();
  	videoOpinie();
  }
  else {
  	$(".js-video").hide();
  	$(".js-video-note").show();
  	videoOpinieStop();
  }
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
	}
}

$(document).ready(function() {

	slick();
	slick2();

	videoScroll();

	footerMarginCookie();

	footerMarginCookie();



	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});


	function initHeaderBtn() {
		var height = 50 ;
		if ( $(window).scrollTop() > height ) {
			$("header").addClass("is-scroll");
		} else { 
			$("header").removeClass("is-scroll");
		}
	}
	initHeaderBtn();

	function firstSlide() {
		var windowHeight = $(window).height();
		var firstSlide = $(".js-landing-slider");
		var howWork = $(".js-how-work").outerHeight();
		var imgHeight = windowHeight - howWork;

		$(".js-how-work").css("bottom", - howWork);
		$("#section2").css("padding-top", howWork);

		firstSlide.css("height", imgHeight); // minheight надо сделать
	}
	firstSlide();


	$(window).scroll(function () {
		initHeaderBtn();
		videoScroll();
	});
	$(window).resize(function () {
		firstSlide();
		footerMarginCookie();
	});


});