$(document).ready(function () {

	// sticky footer
	function stickyFooter() {
		var footerHeight = $("footer").height();
		$(".out").css("margin-bottom", -footerHeight);
		$(".push").css("height", footerHeight);
	}
	stickyFooter();

	$(window).resize(function() {
    stickyFooter();
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
		}
	});

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});
	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

});