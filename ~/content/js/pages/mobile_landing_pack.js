function slick() {
	$('.js-partners').slick({
		dots: false,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-partners-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-partners-next'></i>",
		responsive: [
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]

	});
}

function slick2() {
	$('.js-review').slick({
		dots: true,
		infinite: true,
		draggable: false,
		swipe: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:"<i class='fa fa-angle-left js-review-prev'></i>",
		nextArrow:"<i class='fa fa-angle-right js-review-next'></i>"
	});
}

function videoOpinie() {
	var player = videojs('really-cool-video', { /* Options */ }, function() {
		// this.play();
	});
}

$(document).ready(function() {
	slick();
	slick2();
	videoOpinie();

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".landing-nav").slideToggle(200);
		$(this).toggleClass("is-active");
		$(".nav-overlay").toggle();
	});
	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".landing-nav").slideUp(200);
		$(".js-nav-mobile").removeClass("is-active");
		$(".nav-overlay").hide();
	});

	function initHeaderBtn() {
		var height1 = $("#section1").height();

		if ( $(window).scrollTop() > height1 ) {
			$("header").addClass("m-mobile-scroll");
		} else { 
			$("header").removeClass("m-mobile-scroll");
		}
	}
	initHeaderBtn();

	$(window).scroll(function () {
		initHeaderBtn();
	});

	// $(window).resize(function() {
 //    footerMarginCookie();
	// });
});