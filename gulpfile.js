var gulp = require('gulp'),
		del = require('del'),
		sass = require('gulp-sass'),

		sass_merchant = require('gulp-sass'),
		sass_checkout = require('gulp-sass'),
		sass_general = require('gulp-sass'),
		sass_btn = require('gulp-sass'),

		sass_landing = require('gulp-sass'),
		sass_landing_part2 = require('gulp-sass'),

		sass_landing_birthday = require('gulp-sass'),
		sass_landing_group = require('gulp-sass'),
		sass_landing_wedding = require('gulp-sass'),

		connect = require('gulp-connect'),
		spritesmith  = require('gulp.spritesmith'),
		cssbeautify = require('gulp-cssbeautify'),
		autoprefixer = require('gulp-autoprefixer');

var outputDir = '~/content';
	
// server
gulp.task('connect', function () {
	connect.server({
		root: ['.'],
		port: '8080',
		livereload: true
	});
});

// Scripts
gulp.task('js', function() {
	return gulp.src('~/content/js/pages/*.js')
		.pipe(connect.reload());
});


// html 
gulp.task('html', function() {
	return gulp.src('.')
		.pipe(connect.reload());
});

// Styles allFile
gulp.task('sass', function () {
gulp.src('src/sass/*.sass')
	.pipe(sass())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css'))
	.pipe(connect.reload());
});

// Styles generalFile
gulp.task('sass_general', function () {
gulp.src('src/sass/general/*.sass')
	.pipe(sass_general())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/general'))
	.pipe(connect.reload());
});

// Styles merchantFile
gulp.task('sass_merchant', function () {
gulp.src('src/sass/merchant/*.sass')
	.pipe(sass_merchant())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/merchant'))
	.pipe(connect.reload());
});

// Styles checkoutFile
gulp.task('sass_checkout', function () {
gulp.src('src/sass/checkout/*.sass')
	.pipe(sass_checkout())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/checkout'))
	.pipe(connect.reload());
});

// Styles wishroundBtn
gulp.task('sass_btn', function () {
gulp.src('src/sass/btn/*.sass')
	.pipe(sass_btn())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/btn'))
	.pipe(connect.reload());
});

// Styles landing
gulp.task('sass_landing', function () {
gulp.src('src/sass/landing/*.sass')
	.pipe(sass_landing_birthday())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/landing'))
	.pipe(connect.reload());
});

// Styles landing_birthday
gulp.task('sass_landing_birthday', function () {
gulp.src('src/sass/landing_birthday/*.sass')
	.pipe(sass_landing_birthday())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/landing_birthday'))
	.pipe(connect.reload());
});

// Styles landing_group
gulp.task('sass_landing_group', function () {
gulp.src('src/sass/landing_group/*.sass')
	.pipe(sass_landing_group())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/landing_group'))
	.pipe(connect.reload());
});

// Styles landing
gulp.task('sass_landing_wedding', function () {
gulp.src('src/sass/landing_wedding/*.sass')
	.pipe(sass_landing_wedding())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/landing_wedding'))
	.pipe(connect.reload());
});

// Styles landing_part2
gulp.task('sass_landing_part2', function () {
gulp.src('src/sass/landing_part2/*.sass')
	.pipe(sass_landing_birthday())
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(autoprefixer({
		browsers: ['last 5 version', '> 1%', 'Firefox ESR', 'Opera 12.1', 'ie 8' ],
		cascade: false
	}))
	.pipe(gulp.dest(outputDir + '/css/landing_part2'))
	.pipe(connect.reload());
});

// Sprites
gulp.task('sprite', function() {
	var spriteData =
		gulp.src('src/img/icons/*.png')
			.pipe(spritesmith({
				imgName: 'sprite.png',
				cssName: '_sprite.sass',
				cssFormat: 'sass',
				algorithm: 'binary-tree',
				padding: 5,
				cssTemplate: 'sass.template.mustache',
				cssVarMap: function(sprite) {
					sprite.name = 's-' + sprite.name
				}
			}));
	spriteData.img.pipe(gulp.dest(outputDir + '/img'));
	spriteData.css.pipe(gulp.dest('src/sass/lib'));
});

// Clean outputDir
gulp.task('clean', function (cb) {
	del([
		outputDir,
	], cb);
});

// Watch files for changes
gulp.task('watch', function() {
	gulp.watch('src/img/icons/*.png', ['sprite']);
	gulp.watch('~/content/js/pages/*.js', ['js']);
	gulp.watch('./*.html', ['html']);
	gulp.watch('src/sass/*.{sass,scss}', ['sass']);
	gulp.watch('src/sass/**/*.{sass,scss}', ['sass']);

	gulp.watch('src/sass/merchant/*.sass', ['sass_merchant']);
	gulp.watch('src/sass/checkout/*.sass', ['sass_checkout']);
	gulp.watch('src/sass/general/*.sass', ['sass_general']);
	gulp.watch('src/sass/btn/*.sass', ['sass_btn']);

	gulp.watch('src/sass/landing/*.sass', ['sass_landing']);

	gulp.watch('src/sass/landing_birthday/*.sass', ['sass_landing_birthday']);
	gulp.watch('src/sass/landing_group/*.sass', ['sass_landing_group']);
	gulp.watch('src/sass/landing_wedding/*.sass', ['sass_landing_wedding']);

	gulp.watch('src/sass/landing_part2/*.sass', ['sass_landing_part2']);
});

// Build task
gulp.task('build', [ 'sprite', 'sass', 'sass_merchant', 'sass_checkout', 'sass_general', 'sass_btn', 'sass_landing', 'sass_landing_birthday', 'sass_landing_group', 'sass_landing_wedding', 'sass_landing_part2', 'js' ]);

// Default task
gulp.task('default', [ 'sprite', 'sass','sass_merchant', 'sass_checkout', 'sass_general', 'sass_btn', 'sass_landing', 'sass_landing_birthday', 'sass_landing_group', 'sass_landing_wedding', 'sass_landing_part2', 'js', 'connect', 'watch']);
